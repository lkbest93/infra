packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name      = "bieber-${local.timestamp}"
  instance_type = "t2.micro"
  region        = "ap-northeast-2"
  source_ami = "ami-0ba5cd124d7a79612"
  subnet_id = "subnet-01fd1e8a26721ae45"
  associate_public_ip_address = true
  ssh_interface = "public_ip"
  ssh_username = "ubuntu"
}

build {
  sources = [
    "source.amazon-ebs.ubuntu"
  ]
  provisioner "shell" {
    inline = [ "mkdir server"]
  }
  provisioner "file"{
    source = "app/"
    destination = "server"
  }
  provisioner "shell" {
    inline = [
      "echo Installing Server",
      "sleep 25",
      "sudo apt-get update",  
      "sudo apt-get install -y nodejs npm git", 
      "npm install --prefix ./server",
      "sudo cp /home/ubuntu/server/server.service /lib/systemd/system/server.service",
      "sudo systemctl daemon-reload",
      "sudo systemctl enable server"   
    ]
  }
}
locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "") 
}   