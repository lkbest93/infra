
resource "aws_s3_bucket" "b" {
  bucket = "www.bieber-demo.tk"
  acl    = "public-read"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Id": "Policy1621307566829",
    "Statement": [
        {
            "Sid": "2",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${aws_cloudfront_origin_access_identity.cf.iam_arn}"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::www.bieber-demo.tk/*"
        }
    ]
}
    EOF
  tags = {
    Name = "bieber-web"
  }
  versioning {
    enabled = true
  }
}


locals {
  s3_origin_id = "bieberS3"
}


resource "aws_cloudfront_origin_access_identity" "cf" {
  comment = "bieber cloudfront"
}
resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = aws_s3_bucket.b.bucket_regional_domain_name
    origin_id   = local.s3_origin_id
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.cf.cloudfront_access_identity_path
    }
  }
  restrictions {
    geo_restriction {
      restriction_type = "blacklist"
      locations        = ["US", "CA", "GB", "DE"]
    }
  }
  aliases = ["www.bieber-demo.tk",]
  enabled             = true
  comment             = "bieber"
  default_root_object = "index.html"
  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }
  price_class = "PriceClass_200"
  viewer_certificate {
      acm_certificate_arn = "arn:aws:acm:us-east-1:151564769076:certificate/62dc228b-0e80-4654-baff-e5c18fa2a0b3"
      ssl_support_method = "sni-only"
  }
}

