
/*
ec2
    web
    ec2_was_2a
        
eip
    eip_web_2a
    eip_igw
    eip_nat

igw
    igw

subnet
    private 10.0.1.0/24
    public  10.0.0.0/24
nat
    nat

route table
    private
    public

vpc
    vpc 10.0.0.0/16
*/


terraform {
  required_version = "~> 0.12.26" //테라폼 버전 지정

  backend "s3" {
    bucket = "tfstate-bieber"
    key    = "tfstate"
    region = "ap-northeast-2"
  }
}

provider "aws" {region = "ap-northeast-2"} //access_id와 secret_key는 awscli configuration에서 설정함

//ec2 ssh접속을 위한 public 키
resource "aws_key_pair" "aws_key_pair" {
  key_name   = "aws_key_pair"
  public_key = file("~/.ssh/bieber.pub")
}

//bastion ssh접속을 위한 public 키
resource "aws_key_pair" "aws_key_pair_bastion" {
  key_name   = "aws_key_pair_bastion"
  public_key = file("~/.ssh/bastion.pub")
}

