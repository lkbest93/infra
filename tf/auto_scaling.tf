resource "aws_placement_group" "was" {
  name     = "bieber-was-pg"
  strategy = "partition"
}

resource "aws_launch_template" "was" {
  name_prefix   = "bieber-was-"

  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      volume_size = 8
    }
  }
  update_default_version = true
  key_name = aws_key_pair.aws_key_pair.key_name 
  image_id      = data.aws_ssm_parameter.bieber-was-ami-id.value

  instance_initiated_shutdown_behavior = "terminate"

  instance_type = "t2.micro"


  network_interfaces {
    associate_public_ip_address = false
     security_groups = [
      aws_security_group.allow_vpc_inbound.id,
      aws_security_group.allow_outbound.id,
      aws_security_group.allow_http_https_inbound.id
      ]
  }
  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "bieber-was"
    }
  }
}

resource "aws_autoscaling_group" "was" {
  name                      = "bieber-was-asg"
  max_size                  = 3
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "EC2"
  desired_capacity          = 2
  force_delete              = true
  placement_group           = aws_placement_group.was.id
  launch_template {
    id = aws_launch_template.was.id
    version = aws_launch_template.was.default_version
  }
  vpc_zone_identifier       = [aws_subnet.private.id, aws_subnet.private_c.id]
  wait_for_capacity_timeout = "4m"
  target_group_arns = [aws_lb_target_group.back_end.arn]
instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
  }
  timeouts {
    delete = "15m"
  }
    depends_on = [
        aws_launch_template.was
    ]
}